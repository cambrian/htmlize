//////////////////////////////////////////////////////////////////////////
// The contents of this file are subject to the Mozilla Public License
// Version 1.0 (the "License"); you may not use this file except in
// compliance with the License. You may obtain a copy of the License at
// http://www.mozilla.org/MPL/
//
// Software distributed under the License is distributed on an "AS IS"
// basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
// License for the specific language governing rights and limitations under
// the License.
//
// The Original Code is Html-ization Utility Classes in Java.
//
// The Initial Developer of the Original Code is Hyoungsoo Yoon.
// Portions created by Hyoungsoo Yoon are
// Copyright (C) 1999 Hyoungsoo Yoon.  All Rights Reserved.
//
// Contributor(s):
//
//////////////////////////////////////////////////////////////////////////


/*
  Html-ization Utility Classes

  @copyright  Hyoungsoo Yoon
  @date  Sep 6th, 1999
*/
package com.bluecraft.htmlize;

import java.io.*;
import java.util.*;
import java.net.*;


/**
  HtmlURL extends ProgramURL which implements Htmlizable.
  HtmlURL is a URL to a Html program file.
  This class generates HTML markup-ed output of its file content
  through htmlize(...) functions.

  @author  Hyoungsoo Yoon
  @version  1.1
*/
public class HtmlURL extends SgmlURL {

    //<Static_Fields>
    private static String strKeywords =     // Keywords are case insensitive: These keywords should be stored as uppercases!!!!!!!
    "A ABBR ACRONYM ADDRESS APPLET AREA B BASE BASEFONT BDO BIG " +
    "BLOCKQUOTE BODY BR BUTTON CAPTION CENTER CITE CODE COL COLGROUP " +
    "DD DEL DFN DIR DIV DL DT EM FIELDSET FONT FORM FRAME FRAMESET " +
    "H1 H2 H3 H4 H5 H6 HEAD HR HTML I IFRAME IMG INPUT INS ISINDEX KBD " +
    "LABEL LEGEND LI LINK MAP MENU META NOFRAMES NOSCRIPT OBJECT OL " +
    "OPTGROUP OPTION P PARAM PRE Q S SAMP SCRIPT SELECT SMALL SPAN STRIKE " +
    "STRONG STYLE SUB SUP TABLE TBODY TD TEXTAREA TFOOT TH THEAD TITLE TR " +
    "TT U UL VAR ";
    //</Static_Fields>


    //<Private_Fields>
    //</Private_Fields>


    //<Protectd_Fields>
    //</Protectd_Fields>


    //<Public_Fields>
    //</Public_Fields>


    //<Constructors>
    /**
    Construct HtmlURL from the string representation of URL.
    @param spec - the String to parse as a URL.
    @throws MalformedURLException - If the string specifies an unknown protocol.
    */
    public HtmlURL(String spec) throws MalformedURLException {
        this(new URL(spec));
    }
    /**
    Construct HtmlURL from the URL. It uses the predefined keyword set.
    @param url - the URL specifying the html file.
    */
    public HtmlURL(URL url) {
        this(url, strKeywords);
    }
    /**
    Construct HtmlURL from the URL and the keyword list.
    Note that this constructor is protected.
    @param url - the URL specifying the html file.
    @param keywordsList - Keyword list as a concatenated string.
    */
    protected HtmlURL(URL url, String keywordsList) {
        super(url, keywordsList);
        bCaseSensitive = false;
    }
    //</Constructors>


    //<Private_Methods>
    //</Private_Methods>


    //<Protectd_Methods>
    //</Protectd_Methods>


    //<Public_Methods>
    //</Public_Methods>


    //<Main_Method>
    /**
    This function is provided for testing purposes.
    Please use or modify HtmlURL2Html class.
    Main method takes the name of a URL to be converted,
    and it writes its HTML-ized content to the standard output.
    No error checking is done.
    */
    public static void main(String[] args) {
        HtmlURL pURL;
        try {
            pURL = new HtmlURL(args[0]);
            pURL.htmlize();  // write it to the standard output
        } catch(MalformedURLException exc) {
            exc.printStackTrace();
        }
    }
    //</Main_Method>
}

