//////////////////////////////////////////////////////////////////////////
// The contents of this file are subject to the Mozilla Public License
// Version 1.0 (the "License"); you may not use this file except in
// compliance with the License. You may obtain a copy of the License at
// http://www.mozilla.org/MPL/
//
// Software distributed under the License is distributed on an "AS IS"
// basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
// License for the specific language governing rights and limitations under
// the License.
//
// The Original Code is Html-ization Utility Classes in Java.
//
// The Initial Developer of the Original Code is Hyoungsoo Yoon.
// Portions created by Hyoungsoo Yoon are
// Copyright (C) 1999 Hyoungsoo Yoon.  All Rights Reserved.
//
// Contributor(s):
//
//////////////////////////////////////////////////////////////////////////


/*
  Html-ization Utility Classes

  @copyright  Hyoungsoo Yoon
  @date  Sep 6th, 1999
*/
package com.bluecraft.htmlize;

import java.io.*;


/**
  DtdFileFilter implements FileFilter.
  It accepts either directories or DTD files.
  Accepted extensions: dtd.

  @author  Hyoungsoo Yoon
  @version  1.1
*/
public class DtdFileFilter
    extends AbstractFileFilter
{
    public DtdFileFilter() {
        super();
    }

    public DtdFileFilter(String[] names) {
        super(names);
    }

    public boolean accept(File pathName) {
        if(!super.accept(pathName)) {
            return false;
        }

        if(pathName.isDirectory()) {
            return true;
        } else if(pathName.isFile() &&
            (pathName.getName().endsWith(".dtd"))) {
            return true;
        } else {
            return false;
        }
    }
}

