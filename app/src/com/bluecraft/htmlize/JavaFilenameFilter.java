//////////////////////////////////////////////////////////////////////////
// The contents of this file are subject to the Mozilla Public License
// Version 1.0 (the "License"); you may not use this file except in
// compliance with the License. You may obtain a copy of the License at
// http://www.mozilla.org/MPL/
//
// Software distributed under the License is distributed on an "AS IS"
// basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
// License for the specific language governing rights and limitations under
// the License.
//
// The Original Code is Html-ization Utility Classes in Java.
//
// The Initial Developer of the Original Code is Hyoungsoo Yoon.
// Portions created by Hyoungsoo Yoon are
// Copyright (C) 1999 Hyoungsoo Yoon.  All Rights Reserved.
//
// Contributor(s):
//
//////////////////////////////////////////////////////////////////////////


/*
  Html-ization Utility Classes

  @copyright  Hyoungsoo Yoon
  @date  Sep 6th, 1999
*/
package com.bluecraft.htmlize;

import java.io.*;


/**
  JavaFilenameFilter implements FilenameFilter.
  It accepts Java files.
  Accepted extensions: java.

  @author  Hyoungsoo Yoon
  @version  1.1
*/
public class JavaFilenameFilter
    extends AbstractFilenameFilter
{
    public JavaFilenameFilter() {
        super();
    }

    public JavaFilenameFilter(String[] names) {
        super(names);
    }


    public boolean accept(File dir, String name) {
        if(!super.accept(dir, name)) {
            return false;
        }

        File file = new File(dir,name);
        if(file.isDirectory()) {
            return true;
        } else if(file.isFile() && name.endsWith(".java")) {
            return true;
        } else {
            return false;
        }
    }

}


