//////////////////////////////////////////////////////////////////////////
// The contents of this file are subject to the Mozilla Public License
// Version 1.0 (the "License"); you may not use this file except in
// compliance with the License. You may obtain a copy of the License at
// http://www.mozilla.org/MPL/
//
// Software distributed under the License is distributed on an "AS IS"
// basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
// License for the specific language governing rights and limitations under
// the License.
//
// The Original Code is Html-ization Utility Classes in Java.
//
// The Initial Developer of the Original Code is Hyoungsoo Yoon.
// Portions created by Hyoungsoo Yoon are
// Copyright (C) 2002 Hyoungsoo Yoon.  All Rights Reserved.
//
// Contributor(s):
//
//////////////////////////////////////////////////////////////////////////


/*
  Html-ization Utility Classes

  @copyright  Hyoungsoo Yoon
  @date  Aug 21th, 2002
*/
package com.bluecraft.htmlize;

import java.io.*;
import java.util.*;


/**
  AbstractFilenameFilter implements FilenameFilter.
  It filters out all unwanted directories and files before its subclasses do the actual filtering.

  @author  Hyoungsoo Yoon
  @version  1.1
*/
public abstract class AbstractFilenameFilter
    implements FilenameFilter
{
    // List of files and dirs to be filtered out.
    private List mBlackList = null;

    public AbstractFilenameFilter() {
        mBlackList = new ArrayList();
    }

    public AbstractFilenameFilter(String[] names) {
        mBlackList = Arrays.asList(names);
    }


    public boolean accept(File dir, String name) {
        if(mBlackList.contains(name)) {
            return false;
        } else {
            return true;
        }
    }

}


