//////////////////////////////////////////////////////////////////////////
// The contents of this file are subject to the Mozilla Public License
// Version 1.0 (the "License"); you may not use this file except in
// compliance with the License. You may obtain a copy of the License at
// http://www.mozilla.org/MPL/
//
// Software distributed under the License is distributed on an "AS IS"
// basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
// License for the specific language governing rights and limitations under
// the License.
//
// The Original Code is Html-ization Utility Classes in Java.
//
// The Initial Developer of the Original Code is Hyoungsoo Yoon.
// Portions created by Hyoungsoo Yoon are
// Copyright (C) 1999 Hyoungsoo Yoon.  All Rights Reserved.
//
// Contributor(s):
//
//////////////////////////////////////////////////////////////////////////


/*
  Html-ization Utility Classes

  @copyright  Hyoungsoo Yoon
  @date  Sep 6th, 1999
*/
package com.bluecraft.htmlize;

import java.io.*;
import java.util.*;


/**
  SgmlFile implements Htmlizable.
  It converts all sgml files in the source path
  (recursively, if a directory is given) into HTML files.

  @author  Hyoungsoo Yoon
  @version  1.1
*/
public class SgmlFile extends File implements Htmlizable {

    //<Static_Fields>
    private static final int buffMax = 4096;
    private static String strKeywords = " ";
    //</Static_Fields>


    //<Private_Fields>
    private boolean bShowLineNumber = false;
    private String _colorBackground = "ivory";
    private String _colorLineNumber = "maroon";
    private String _colorText = "black";
    private String _colorDescription = "maroon";
    private String _colorString = "red";
    private String _colorEntity = "maroon";
    private String _colorComment = "green";
    private String _colorKeyword = "blue";
    private String _colorUserKeyword = "purple";
    private boolean _boldString = false;
    private boolean _boldEntity = false;
    private boolean _boldComment = false;
    private boolean _boldKeyword = false;
    private boolean _boldUserKeyword = true;

    private int currentLineNumber = 1;
    private char[] buff = new char[buffMax];
    private int currentPos = 0;
    private String _descriptionPhrase = "";
    //</Private_Fields>


    //<Protectd_Fields>
    protected Collection KEYWORDS = new HashSet();
    protected Collection UserKeywords = new HashSet();
    protected boolean bCaseSensitive = true;
    //</Protectd_Fields>


    //<Public_Fields>
    //</Public_Fields>


    //<Constructors>
    /**
    @param parent Pathname in which the file is created.
    @param name Name of the file to be created.
    */
    public SgmlFile(String parent, String name) {
        super(parent, name);

        StringTokenizer st = new StringTokenizer(strKeywords);
        while (st.hasMoreTokens()) {
            KEYWORDS.add(st.nextToken());
        }
    }
    /**
    @param parent File object in which the file is created.
    @param name Name of the file to be created.
    */
    public SgmlFile(File parent, String name) {
        super(parent, name);

        StringTokenizer st = new StringTokenizer(strKeywords);
        while (st.hasMoreTokens()) {
            KEYWORDS.add(st.nextToken());
        }
    }
    /**
    @param pathname Full pathname of the file to be created.
    */
    public SgmlFile(String pathname) {
        super(pathname);

        StringTokenizer st = new StringTokenizer(strKeywords);
        while (st.hasMoreTokens()) {
            KEYWORDS.add(st.nextToken());
        }
    }
    //</Constructors>


    //<Private_Methods>
    //</Private_Methods>


    //<Protectd_Methods>
    protected void printChar(PrintWriter out, int c) {
        if(c == '\n') {
            out.print('\n');
            if(bShowLineNumber) {
                printLineNumber(out);
            }
        } else if (c == '<') {
            out.print("&lt;");
        } else if (c == '>') {
            out.print("&gt;");
        } else if (c == '&') {
            out.print("&amp;");
        } else if (c == '\r') {
            ;  // Eat this up!
        } else {
            out.print((char) c);
        }
    }

    protected void printLine(PrintWriter out, String strLine) {
        out.print(strLine);
        out.print('\n');
    }

    protected void printLineNumber(PrintWriter out) {
        out.print("<SPAN class=\"linenumber\">");
        if(currentLineNumber <= 9) {
            out.print("" + currentLineNumber++ + "  : ");
        } else if(currentLineNumber <= 99) {
            out.print("" + currentLineNumber++ + " : ");
        } else {
            out.print("" + currentLineNumber++ + ": ");
        }
        out.print("</SPAN>");
    }

    protected void writeStyle(PrintWriter out) {
        printLine(out, "<STYLE>");
        printLine(out, "body {");
        printLine(out, "    background: " + _colorBackground + ";");
        printLine(out, "    color: " + _colorText + ";");
        printLine(out, "}");
        printLine(out, "DIV.description {");
        printLine(out, "    color: " + _colorDescription + ";");
        //printLine(out, "    font-size: 10pt;");
        printLine(out, "    margin-top: -0.4cm;");
        printLine(out, "}");
        printLine(out, "SPAN.linenumber {");
        printLine(out, "    color: " + _colorLineNumber + ";");
        printLine(out, "}");
        printLine(out, "SPAN.string {");
        printLine(out, "    color: " + _colorString + ";");
        if(_boldString) printLine(out, "    font-weight: bold;");
        printLine(out, "}");
        printLine(out, "SPAN.entity {");
        printLine(out, "    color: " + _colorEntity + ";");
        if(_boldEntity) printLine(out, "    font-weight: bold;");
        printLine(out, "}");
        printLine(out, "SPAN.comment {");
        printLine(out, "    color: " + _colorComment + ";");
        if(_boldComment) printLine(out, "    font-weight: bold;");
        printLine(out, "}");
        printLine(out, "SPAN.keyword {");
        printLine(out, "    color: " + _colorKeyword + ";");
        if(_boldKeyword) printLine(out, "    font-weight: bold;");
        printLine(out, "}");
        printLine(out, "SPAN.userkeyword {");
        printLine(out, "    color: " + _colorUserKeyword + ";");
        if(_boldUserKeyword) printLine(out, "    font-weight: bold;");
        printLine(out, "}");
        printLine(out, "</STYLE>");
    }

    protected void writeProlog(PrintWriter out) {
        printLine(out, "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\">");
        printLine(out, "<HTML lang=\"en\"><HEAD>");
        printLine(out, "<META http-equiv=\"Generator\" content=\"com.bluecraft.htmlize\">");
        printLine(out, "<TITLE>Document: " + getName() + "</TITLE>");
        writeStyle(out);
        printLine(out, "</HEAD><BODY>");
        printLine(out, "<H2>" + getName() + "</H2>");
        if(descriptionPhrase().length() > 0) {
            printLine(out, "<DIV class=\"description\">");
            printLine(out, descriptionPhrase());
            printLine(out, "</DIV>");
        }
        printLine(out, "<PRE>");
    }

    protected void writeEpilog(PrintWriter out) {
        printLine(out, "</PRE>");
        printLine(out, "</BODY></HTML>");
    }

    protected boolean isAlpha(int c) {
        return ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'));
    }

    protected void flushPrinter(PrintWriter out) {
        if (currentPos > 0) {
            String word = new String(buff, 0, currentPos);

            // Note that user-defined keywords take precedence.
            if (UserKeywords.contains(word)) {
                out.print("<SPAN class=\"userkeyword\">");
                out.print(word);
                out.print("</SPAN>");
            } else if ((KEYWORDS.contains(word))
                || ((bCaseSensitive==false) && (KEYWORDS.contains(word.toUpperCase())))) {  // If keywords are case insensitive!!!!!!!!
                out.print("<SPAN class=\"keyword\">");
                out.print(word);
                out.print("</SPAN>");
            } else {
                out.print(word);
            }
            currentPos = 0;
        }
    }

    protected void writeHtml(PrintWriter out)
            throws FileNotFoundException, IOException {

        LineNumberReader lnin =
            new LineNumberReader(
                new FileReader(getPath()));
        BufferedReader in =
            new BufferedReader(lnin);
        writeProlog(out);
        if(showLineNumber()) {
            printLineNumber(out);
        }

        int c;
        while ((c = in.read())!= -1) {
            if (isAlpha(c)) {
                buff[currentPos++] = (char) c;
            } else {
                flushPrinter(out);
                //if (c == '"' || c == '\'') {
                if (c == '"') {   // Todo: How to handle apostrophe???
                    out.print("<SPAN class=\"string\">");
                    int endQuote = c;
                    printChar(out, c);
                    while ((c = in.read()) != -1 && c != endQuote) {
                        if (c == '\\') {
                            printChar(out, c);
                            c = in.read();
                        }
                        printChar(out, c);
                    }
                    printChar(out, c);
                    out.print("</SPAN>");
                } else if (c == '&') {
                    if (((c = in.read()) != -1) && (c != ' ')
                        && (c != '\t') && (c != '\n') && (c != '\r')) {
                        out.print("<SPAN class=\"entity\">");
                        int endEntity = ';';
                        out.print('&');
                        printChar(out, c);
                        while ((c = in.read()) != -1 && (c != endEntity)) {
                            printChar(out, c);
                        }
                        printChar(out, c);
                        out.print("</SPAN>");
                    } else {
                        out.print('&');
                        printChar(out, c);
                    }
                } else if (c == '%') {
                    if (((c = in.read()) != -1) && (c != ' ')
                        && (c != '\t') && (c != '\n') && (c != '\r')) {
                        out.print("<SPAN class=\"entity\">");
                        int endEntity = ';';
                        out.print('%');
                        printChar(out, c);
                        while ((c = in.read()) != -1 && (c != endEntity)) {
                            printChar(out, c);
                        }
                        printChar(out, c);
                        out.print("</SPAN>");
                    } else {
                        out.print('%');
                        printChar(out, c);
                    }
                } else if (c == '<') {
                    if (((c = in.read()) != -1) && (c == '!')) {
                        if (((c = in.read()) != -1) && (c == '-')) {
                            out.print("<SPAN class=\"comment\">");
                            out.print("&lt;!-");
                            int prevChar = c;
                            while ((c = in.read()) != -1) {
                                printChar(out, c);
                                if(prevChar == '-' && c == '>') {
                                    break;
                                }
                                prevChar = c;
                            }
                            out.print("</SPAN>");
                        } else {
                            out.print("&lt;!");
                            if (isAlpha(c)) {
                                 buff[currentPos++] = (char) c;
                            } else {
                                printChar(out, c);
                            }
                        }
                    } else {
                        out.print("&lt;");
                        if (isAlpha(c)) {
                             buff[currentPos++] = (char) c;
                        } else {
                             printChar(out, c);
                        }
                    }
                } else {
                    printChar(out, c);
                }
            }
        }
        flushPrinter(out);
        writeEpilog(out);
        in.close();
    }

    protected void generateIndexFile(File destDir, String[] childList) {
        String indexName = destDir.getName() + "_sgml_index.html";
        File indexFile = new File(destDir,indexName);
        try {
            PrintWriter out =
                new PrintWriter(
                    new BufferedWriter(
                        new FileWriter(indexFile)));
            printLine(out, "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\">");
            printLine(out, "<HTML lang=\"en\"><HEAD>");
            printLine(out, "<META http-equiv=\"Generator\" content=\"com.bluecraft.htmlize\">");
            printLine(out, "<TITLE>Source Directory: " + getName() + "</TITLE>");
            printLine(out, "<STYLE>");
            printLine(out, "body {");
            printLine(out, "    background: " + _colorBackground + ";");
            printLine(out, "    color: " + _colorText + ";");
            printLine(out, "}");
            printLine(out, "DIV.description {");
            printLine(out, "    color: " + _colorDescription + ";");
            //printLine(out, "    font-size: 10pt;");
            printLine(out, "    margin-top: -0.4cm;");
            printLine(out, "}");
            printLine(out, "DL DT {");
            printLine(out, "    font-size: 1.1em;");
            printLine(out, "    margin-left: 0.2cm;");
            printLine(out, "}");
            printLine(out, "</STYLE>");
            printLine(out, "</HEAD><BODY>");
            printLine(out, "<H2>Index of Directory: " + getName() + "</H2>");
            if(descriptionPhrase().length() > 0) {
                printLine(out, "<DIV class=\"description\">");
                printLine(out, descriptionPhrase());
                printLine(out, "</DIV>");
            }
            printLine(out, "<DIV><DL>");
            printLine(out, "<!-- Write descriptions inside <DD> elements -->");

            SgmlFile jFile;
            String linkName;
            String fileName;
            for(int i=0;i<childList.length;i++) {
                jFile = new SgmlFile(getPath(),childList[i]);
                if(jFile.isDirectory()) {
                    linkName = childList[i] + "/" +
                        childList[i] + "_sgml_index.html";
                    fileName = "[" + childList[i] + "]";
                    out.print("<DT><A href=\"" + linkName + "\">");
                    out.print(fileName);
                    printLine(out, "</A></DT>");
                    printLine(out, "<DD>");
                    printLine(out, "</DD>");
                }
            }
            for(int i=0;i<childList.length;i++) {
                jFile = new SgmlFile(getPath(),childList[i]);
                if(!jFile.isDirectory()) {
                    int dot = childList[i].lastIndexOf('.');
                    linkName = childList[i].substring(0,dot) +
                        "_" + childList[i].substring(dot+1) + ".html";
                    fileName = childList[i];
                    out.print("<DT><A href=\"" + linkName + "\">");
                    out.print(fileName);
                    printLine(out, "</A></DT>");
                    printLine(out, "<DD>");
                    printLine(out, "</DD>");
                }
            }
            printLine(out, "</DL></DIV>");
            printLine(out, "</BODY></HTML>");
            out.close();
        } catch(FileNotFoundException exc) {
            System.out.println("File Not Found: " + getPath());
        } catch(IOException exc) {
            exc.printStackTrace();
        }
    }
    //</Protectd_Methods>


    //<Public_Methods>
    /**
    Copy private attributes of this SgmlFile to another file.
    @param pFile SgmlFile to which the attributes is to be copied.
    */
    public void copyAttributesTo(SgmlFile pFile) {
        pFile.showLineNumber(bShowLineNumber);
        pFile.lineNumberColor(_colorLineNumber);
        pFile.backgroundColor(_colorBackground);
        pFile.textColor(_colorText);
        pFile.descriptionColor(_colorDescription);
        pFile.stringColor(_colorString);
        pFile.entityColor(_colorEntity);
        pFile.commentColor(_colorComment);
        pFile.keywordColor(_colorKeyword);
        pFile.userKeywordColor(_colorUserKeyword);
        pFile.boldString(_boldString);
        pFile.boldEntity(_boldEntity);
        pFile.boldComment(_boldComment);
        pFile.boldKeyword(_boldKeyword);
        pFile.boldUserKeyword(_boldUserKeyword);
        pFile.descriptionPhrase(_descriptionPhrase);
    }
    /**
    Copy private attributes from another SgmlFile.
    @param pFile SgmlFile from which the attributes is to be copied.
    */
    public void copyAttributesFrom(SgmlFile pFile) {
        bShowLineNumber = pFile.showLineNumber();
        _colorLineNumber = pFile.lineNumberColor();
        _colorBackground = pFile.backgroundColor();
        _colorText = pFile.textColor();
        _colorDescription = pFile.descriptionColor();
        _colorString = pFile.stringColor();
        _colorEntity = pFile.entityColor();
        _colorComment = pFile.commentColor();
        _colorKeyword = pFile.keywordColor();
        _colorUserKeyword = pFile.userKeywordColor();
        _boldString = pFile.boldString();
        _boldEntity = pFile.boldEntity();
        _boldComment = pFile.boldComment();
        _boldKeyword = pFile.boldKeyword();
        _boldUserKeyword = pFile.boldUserKeyword();
        _descriptionPhrase = pFile.descriptionPhrase();
    }

    /**
    Toggle line number mode.
    @param bShow If true, show line numbers.
    */
    public void showLineNumber(boolean bShow) {
        bShowLineNumber = bShow;
    }
    /**
    Get the current line number mode.
    */
    public boolean showLineNumber() {
        return bShowLineNumber;
    }

    /**
    Set background color to _color.
    @param _color New background color.
    Use the color name or 6 hexadecimal numbers (e.g. #00ff00 indicates green).
    */
    public void backgroundColor(String _color) {
        _colorBackground = _color;
    }
    /**
    Get the current background color.
    @return Background color.
    */
    public String backgroundColor() {
        return _colorBackground;
    }
    /**
    Set text color to _color.
    @param _color New text color.
    Use the color name or 6 hexadecimal numbers (e.g. #00ff00 indicates green).
    */
    public void textColor(String _color) {
        _colorText = _color;
    }
    /**
    Get the current text color.
    @return Text color.
    */
    public String textColor() {
        return _colorText;
    }
    /**
    Set description phrase color to _color.
    @param _color New description phrase color.
    Use the color name or 6 hexadecimal numbers (e.g. #00ff00 indicates green).
    */
    public void descriptionColor(String _color) {
        _colorDescription = _color;
    }
    /**
    Get the current description phrase color.
    @return Description phrase color.
    */
    public String descriptionColor() {
        return _colorDescription;
    }
    /**
    Set line number color to _color.
    @param _color New line number color.
    Use the color name or 6 hexadecimal numbers (e.g. #00ff00 indicates green).
    */
    public void lineNumberColor(String _color) {
        _colorLineNumber = _color;
    }
    /**
    Get the current line number color.
    @return Line number color.
    */
    public String lineNumberColor() {
        return _colorLineNumber;
    }
    /**
    Set string color to _color.
    @param _color New string color.
    Use the color name or 6 hexadecimal numbers (e.g. #00ff00 indicates green).
    */
    public void stringColor(String _color) {
        _colorString = _color;
    }
    /**
    Get the current string color.
    @return String color.
    */
    public String stringColor() {
        return _colorString;
    }
    /**
    Set entity color to _color.
    Currently, same color is used for both
    parameter entities and character entities.
    @param _color New entity color.
    Use the color name or 6 hexadecimal numbers (e.g. #00ff00 indicates green).
    */
    public void entityColor(String _color) {
        _colorEntity = _color;
    }
    /**
    Get the current entity color.
    Currently, same color is used for both
    parameter entities and character entities.
    @return Entity color.
    */
    public String entityColor() {
        return _colorEntity;
    }
    /**
    Set comment color to _color.
    @param _color New comment color.
    Use the color name or 6 hexadecimal numbers (e.g. #00ff00 indicates green).
    */
    public void commentColor(String _color) {
        _colorComment = _color;
    }
    /**
    Get the current comment color.
    @return Comment color
    */
    public String commentColor() {
        return _colorComment;
    }
    /**
    Set keyword color to _color.
    @param _color New keyword color.
    Use the color name or 6 hexadecimal numbers (e.g. #00ff00 indicates green).
    */
    public void keywordColor(String _color) {
        _colorKeyword = _color;
    }
    /**
    Get the current keyword color.
    @return Keyword color
    */
    public String keywordColor() {
        return _colorKeyword;
    }
    /**
    Set user-defined keyword color to _color.
    @param _color New user-defined keyword color.
    Use the color name or 6 hexadecimal numbers (e.g. #00ff00 indicates green).
    */
    public void userKeywordColor(String _color) {
        _colorUserKeyword = _color;
    }
    /**
    Get the current user-defined keyword color.
    @return User-defined keyword color
    */
    public String userKeywordColor() {
        return _colorUserKeyword;
    }

    /**
    Add a new keyword to the set of user-defined keywords.
    Note that user-defined keywords take precedence over sgml keywords.
    @param keyword New keyword to be added to the set.
    */
    public void addUserKeywords(String keyword) {
        UserKeywords.add(keyword);
    }
    /**
    Add a new keyword to the set of user-defined keywords.
    Note that user-defined keywords take precedence over sgml keywords.
    @param keywords New keyword array to be added to the set.
    */
    public void addUserKeywords(String[] keywords) {
        for(int i=0;i<keywords.length;i++) {
            UserKeywords.add(keywords[i]);
        }
    }
    /**
    Clear the set of user-defined keywords.
    Note that user-defined keywords take precedence over sgml keywords.
    */
    public void clearUserKeywordSet() {
        UserKeywords.clear();
    }
    /**
    Get the set of user-defined keywords.
    Note that user-defined keywords take precedence over sgml keywords.
    */
    public String[] userKeywordSet() {
        return (String[]) UserKeywords.toArray();
    }
    /**
    Set the set of user-defined keywords.
    Note that user-defined keywords take precedence over sgml keywords.
    WARNING: It automatically removes all existing keywords.
    If you'd like to keep the old keywords, use addUserKeywords(String[]).
    */
    public void userKeywordSet(String[] keywords) {
        UserKeywords.clear();
        for(int i=0;i<keywords.length;i++) {
            UserKeywords.add(keywords[i]);
        }
    }

    /**
    Toggle string font weight (bold vs plain).
    @param _bold If true, set strings to bold.
    */
    public void boldString(boolean _bold) {
        _boldString = _bold;
    }
    /**
    Get the current font weight of strings.
    @return String font weight.
    */
    public boolean boldString() {
        return _boldString;
    }
    /**
    Toggle entity font weight (bold vs plain).
    @param _bold If true, set entities to bold.
    */
    public void boldEntity(boolean _bold) {
        _boldEntity = _bold;
    }
    /**
    Get the current font weight of entities.
    @return Entity font weight.
    */
    public boolean boldEntity() {
        return _boldEntity;
    }
    /**
    Toggle comment font weight (bold vs plain).
    @param _bold If true, set comments to bold.
    */
    public void boldComment(boolean _bold) {
        _boldComment = _bold;
    }
    /**
    Get the current font weight of comments.
    @return Comment font weight.
    */
    public boolean boldComment() {
        return _boldComment;
    }
    /**
    Toggle keyword font weight (bold vs plain).
    @param _bold If true, set keywords to bold.
    */
    public void boldKeyword(boolean _bold) {
        _boldKeyword = _bold;
    }
    /**
    Get the current font weight of the keywords.
    @return Keyword font weight.
    */
    public boolean boldKeyword() {
        return _boldKeyword;
    }
    /**
    Toggle user-defined keyword font weight (bold vs plain).
    @param _bold If true, set user-defined keywords to bold.
    */
    public void boldUserKeyword(boolean _bold) {
        _boldUserKeyword = _bold;
    }
    /**
    Get the current font weight of the user-defined keywords.
    @return User-defined keyword font weight.
    */
    public boolean boldUserKeyword() {
        return _boldUserKeyword;
    }

    /**
    Get the current description phrase.
    @return Description phrase.
    */
    public String descriptionPhrase() {
        return _descriptionPhrase;
    }
    /**
    Set the description phrase.
    @param newDescription New description phrase.
    */
    public void descriptionPhrase(String newDescription) {
        _descriptionPhrase = newDescription;
    }

    /**
    Htmlize the file and write its html file to the standard output
    This file should be a SgmlFile.
    */
    public void htmlize() {
        FileFilter progFilter = new SgmlFileFilter();
        if(!isFile() || !progFilter.accept(this)) {
            System.err.println("Source path name not valid");
            return;
        }

        // write to the standard output
        try {
            PrintWriter out =
                new PrintWriter(
                    new BufferedWriter(
                        new OutputStreamWriter(System.out)));
            writeHtml(out);
            out.close();
        } catch(FileNotFoundException exc) {
            System.out.println("File Not Found: " + getName());
        } catch(IOException exc) {
            exc.printStackTrace();
        }
    }

    /**
    Write this file to the PrintWriter as Html file
    This file should be a SgmlFile.
    @param out PrintWriter to which the output is to be written.
    */
    public void htmlize(PrintWriter out) {
        FileFilter progFilter = new SgmlFileFilter();
        if(!isFile() || !progFilter.accept(this)) {
            System.err.println("Source path name not valid");
            return;
        }

        try {
            writeHtml(out);
            out.close();
        } catch(FileNotFoundException exc) {
            System.out.println("File Not Found: " + getName());
        } catch(IOException exc) {
            exc.printStackTrace();
        }
    }

    /**
    Htmlize the file and write its html file to the destination.
    If this file is a directory, the destination should also be a directory.
    If this file is a SgmlFile, the destination should be an html file,
    or a directory in which the converted file is to be written.
    WARNING: If the same name as the destination file exists, it OVERWRITES!
    @param destPath Destination file or directory
    */
    public void htmlize(String destPath) {
        if(destPath == null) {
            // set destPath to the current directory if "this" is dir
            // or set it to the current directory + name + ".html"
        }

        FileFilter progFilter = new SgmlFileFilter();
        if(isDirectory()) {
            String[] childList = list(new SgmlFilenameFilter());
            File dFile = new File(destPath); // should be directory!!!!
            if(!dFile.exists()) {
                dFile.mkdir();
            }
            if(!dFile.isDirectory()) {
                System.err.println("Destination path name not valid: " +
                    dFile.getPath() + " should be a directory.");
                return;
            }

            generateIndexFile(dFile,childList);

            for(int i=0;i<childList.length;i++) {
                SgmlFile jFile = new SgmlFile(getPath(),childList[i]);
                copyAttributesTo(jFile);
                if(jFile.isDirectory()) {
                    jFile.htmlize(dFile.getPath() + separator + childList[i]);
                } else {
                    int dot = childList[i].lastIndexOf('.');
                    String htmlName = childList[i].substring(0,dot) +
                        "_" + childList[i].substring(dot+1) + ".html";
                    jFile.htmlize(dFile.getPath() + separator + htmlName);
                }
            }
            return;
        } else if(isFile() && progFilter.accept(this)) {
            SgmlFile dstFile = new SgmlFile(destPath);
            if(dstFile.isDirectory()) {
                int dot = getName().lastIndexOf('.');
                String htmlName = getName().substring(0,dot) +
                    "_" + getName().substring(dot+1) + ".html";
                destPath = dstFile.getPath() + separator + htmlName;
            } else if(!destPath.endsWith(".htm")
                && !destPath.endsWith(".html")) {
                System.err.println("Destination file name not valid");
                return;
            }
        } else {
            System.err.println("Source path name not valid");
            return;
        }

        try {
            PrintWriter out =
                new PrintWriter(
                    new BufferedWriter(
                        new FileWriter(destPath)));
            writeHtml(out);
            out.close();
        } catch(FileNotFoundException exc) {
            System.out.println("File Not Found: " + getPath());
        } catch(IOException exc) {
            exc.printStackTrace();
        }
    }
    //</Public_Methods>


    //<Main_Method>
    /**
    This function is provided for testing purposes.
    Please use or modify SgmlToHtml class.
    Main method takes the name of a source file or directory to be converted,
    it writes its HTML-ized content to the standard output.
    No error checking is done.
    */
    public static void main(String[] args) {
        SgmlFile pFile = new SgmlFile(args[0]);
        pFile.htmlize();  // write it to the standard output
    }
    //</Main_Method>
}

