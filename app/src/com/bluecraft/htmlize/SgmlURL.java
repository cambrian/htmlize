//////////////////////////////////////////////////////////////////////////
// The contents of this file are subject to the Mozilla Public License
// Version 1.0 (the "License"); you may not use this file except in
// compliance with the License. You may obtain a copy of the License at
// http://www.mozilla.org/MPL/
//
// Software distributed under the License is distributed on an "AS IS"
// basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
// License for the specific language governing rights and limitations under
// the License.
//
// The Original Code is Html-ization Utility Classes in Java.
//
// The Initial Developer of the Original Code is Hyoungsoo Yoon.
// Portions created by Hyoungsoo Yoon are
// Copyright (C) 1999 Hyoungsoo Yoon.  All Rights Reserved.
//
// Contributor(s):
//
//////////////////////////////////////////////////////////////////////////


/*
  Html-ization Utility Classes

  @copyright  Hyoungsoo Yoon
  @date  Sep 6th, 1999
*/
package com.bluecraft.htmlize;

import java.io.*;
import java.util.*;
import java.net.*;


/**
  SgmlURL implements Htmlizable.
  SgmlURL does not have keyword sets.
  It converts the sgml file specified by the URL into HTML stream.

  @author  Hyoungsoo Yoon
  @version  1.1
*/
public class SgmlURL implements Htmlizable {

    //<Static_Fields>
    private static final int buffMax = 4096;
    private static String strKeywords = " ";  // Empty.....
    //</Static_Fields>


    //<Private_Fields>
    private boolean bShowLineNumber = false;
    private String _colorBackground = "ivory";
    private String _colorLineNumber = "maroon";
    private String _colorText = "black";
    private String _colorDescription = "maroon";
    private String _colorString = "red";
    private String _colorEntity = "maroon";
    private String _colorComment = "green";
    private String _colorKeyword = "blue";
    private String _colorUserKeyword = "purple";
    private boolean _boldString = false;
    private boolean _boldEntity = false;
    private boolean _boldComment = false;
    private boolean _boldKeyword = false;
    private boolean _boldUserKeyword = true;

    private int currentLineNumber = 1;
    private char[] buff = new char[buffMax];
    private int currentPos = 0;
    private String _descriptionPhrase = "";
    //</Private_Fields>


    //<Protectd_Fields>
    protected Collection KEYWORDS = new HashSet();
    protected Collection UserKeywords = new HashSet();
    protected boolean bCaseSensitive = true;

    // It would have been nice if SgmlURL could be derived from URL
    // like SgmlFile is derived from File.
    // But, unfortunately java.net.URL is a final class, and hence
    // we use URL as a member variable.
    // But, actually aggregation may be preferred to inheritance in this case.
    protected URL fileURL = null;
    //</Protectd_Fields>


    //<Public_Fields>
    //</Public_Fields>


    //<Constructors>
    /**
    Construct JavaURL from the string representation of URL.
    @param spec - the String to parse as a URL.
    @throws MalformedURLException - If the string specifies an unknown protocol.
    */
    public SgmlURL(String spec) throws MalformedURLException {
        this(new URL(spec));
    }
    /**
    Construct SgmlURL from the URL. It uses the empty keyword set.
    @param url - the URL specifying the sgml file.
    */
    public SgmlURL(URL url) {
        this(url, strKeywords);
    }
    /**
    Construct SgmlURL from the URL and the keyword list.
    Note that this constructor is protected.
    @param url - the URL specifying the sgml file.
    @param keywordsList - Keyword list as a concatenated string.
    */
    protected SgmlURL(URL url, String keywordsList) {
        fileURL = url;

        StringTokenizer st = new StringTokenizer(keywordsList);
        while (st.hasMoreTokens()) {
            KEYWORDS.add(st.nextToken());
        }
    }
    //</Constructors>


    //<Private_Methods>
    //</Private_Methods>


    //<Protectd_Methods>
    protected void printChar(PrintWriter out, int c) {
        if(c == '\n') {
            out.print('\n');
            if(bShowLineNumber) {
                printLineNumber(out);
            }
        } else if (c == '<') {
            out.print("&lt;");
        } else if (c == '>') {
            out.print("&gt;");
        } else if (c == '&') {
            out.print("&amp;");
        } else if (c == '\r') {
            ;  // Eat this up!
        } else {
            out.print((char) c);
        }
    }

    protected void printLine(PrintWriter out, String strLine) {
        out.print(strLine);
        out.print('\n');
    }

    protected void printLineNumber(PrintWriter out) {
        out.print("<SPAN class=\"linenumber\">");
        if(currentLineNumber <= 9) {
            out.print("" + currentLineNumber++ + "  : ");
        } else if(currentLineNumber <= 99) {
            out.print("" + currentLineNumber++ + " : ");
        } else {
            out.print("" + currentLineNumber++ + ": ");
        }
        out.print("</SPAN>");
    }

    protected void writeStyle(PrintWriter out) {
        printLine(out, "<STYLE>");
        printLine(out, "body {");
        printLine(out, "    background: " + _colorBackground + ";");
        printLine(out, "    color: " + _colorText + ";");
        printLine(out, "}");
        printLine(out, "DIV.description {");
        printLine(out, "    color: " + _colorDescription + ";");
        //printLine(out, "    font-size: 10pt;");
        printLine(out, "    margin-top: -0.4cm;");
        printLine(out, "}");
        printLine(out, "SPAN.linenumber {");
        printLine(out, "    color: " + _colorLineNumber + ";");
        printLine(out, "}");
        printLine(out, "SPAN.string {");
        printLine(out, "    color: " + _colorString + ";");
        if(_boldString) printLine(out, "    font-weight: bold;");
        printLine(out, "}");
        printLine(out, "SPAN.entity {");
        printLine(out, "    color: " + _colorEntity + ";");
        if(_boldEntity) printLine(out, "    font-weight: bold;");
        printLine(out, "}");
        printLine(out, "SPAN.comment {");
        printLine(out, "    color: " + _colorComment + ";");
        if(_boldComment) printLine(out, "    font-weight: bold;");
        printLine(out, "}");
        printLine(out, "SPAN.keyword {");
        printLine(out, "    color: " + _colorKeyword + ";");
        if(_boldKeyword) printLine(out, "    font-weight: bold;");
        printLine(out, "}");
        printLine(out, "SPAN.userkeyword {");
        printLine(out, "    color: " + _colorUserKeyword + ";");
        if(_boldUserKeyword) printLine(out, "    font-weight: bold;");
        printLine(out, "}");
        printLine(out, "</STYLE>");
    }

    protected void writeProlog(PrintWriter out) {
        printLine(out, "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\">");
        printLine(out, "<HTML lang=\"en\"><HEAD>");
        printLine(out, "<META http-equiv=\"Generator\" content=\"com.bluecraft.htmlize\">");
        printLine(out, "<TITLE>Document: " + fileURL.toExternalForm() + "</TITLE>");
        writeStyle(out);
        printLine(out, "</HEAD><BODY>");
        printLine(out, "<H2>" + fileURL.toExternalForm() + "</H2>");
        if(descriptionPhrase().length() > 0) {
            printLine(out, "<DIV class=\"description\">");
            printLine(out, descriptionPhrase());
            printLine(out, "</DIV>");
        }
        printLine(out, "<PRE>");
    }

    protected void writeEpilog(PrintWriter out) {
        printLine(out, "</PRE>");
        printLine(out, "</BODY></HTML>");
    }

    protected boolean isAlpha(int c) {
        return ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'));
    }

    protected void flushPrinter(PrintWriter out) {
        if (currentPos > 0) {
            String word = new String(buff, 0, currentPos);

            // Note that user-defined keywords take precedence.
            if (UserKeywords.contains(word)) {
                out.print("<SPAN class=\"userkeyword\">");
                out.print(word);
                out.print("</SPAN>");
            } else if ((KEYWORDS.contains(word))
                || ((bCaseSensitive==false) && (KEYWORDS.contains(word.toUpperCase())))) {  // If keywords are case insensitive!!!!!!!!
                out.print("<SPAN class=\"keyword\">");
                out.print(word);
                out.print("</SPAN>");
            } else {
                out.print(word);
            }
            currentPos = 0;
        }
    }

    protected void writeHtml(PrintWriter out)
            throws FileNotFoundException, IOException {

        BufferedReader in =
            new BufferedReader(
                new InputStreamReader(fileURL.openStream()));
        writeProlog(out);
        if(showLineNumber()) {
            printLineNumber(out);
        }

        int c;
        while ((c = in.read())!= -1) {
            if (isAlpha(c)) {
                buff[currentPos++] = (char) c;
            } else {
                flushPrinter(out);
                //if (c == '"' || c == '\'') {
                if (c == '"') {   // Todo: How to handle apostrophe???
                    out.print("<SPAN class=\"string\">");
                    int endQuote = c;
                    printChar(out, c);
                    while ((c = in.read()) != -1 && c != endQuote) {
                        if (c == '\\') {
                            printChar(out, c);
                            c = in.read();
                        }
                        printChar(out, c);
                    }
                    printChar(out, c);
                    out.print("</SPAN>");
                } else if (c == '&') {
                    if (((c = in.read()) != -1) && (c != ' ')
                        && (c != '\t') && (c != '\n') && (c != '\r')) {
                        out.print("<SPAN class=\"entity\">");
                        int endEntity = ';';
                        out.print('&');
                        printChar(out, c);
                        while ((c = in.read()) != -1 && (c != endEntity)) {
                            printChar(out, c);
                        }
                        printChar(out, c);
                        out.print("</SPAN>");
                    } else {
                        out.print('&');
                        printChar(out, c);
                    }
                } else if (c == '%') {
                    if (((c = in.read()) != -1) && (c != ' ')
                        && (c != '\t') && (c != '\n') && (c != '\r')) {
                        out.print("<SPAN class=\"entity\">");
                        int endEntity = ';';
                        out.print('%');
                        printChar(out, c);
                        while ((c = in.read()) != -1 && (c != endEntity)) {
                            printChar(out, c);
                        }
                        printChar(out, c);
                        out.print("</SPAN>");
                    } else {
                        out.print('%');
                        printChar(out, c);
                    }
                } else if (c == '<') {
                    if (((c = in.read()) != -1) && (c == '!')) {
                        if (((c = in.read()) != -1) && (c == '-')) {
                            out.print("<SPAN class=\"comment\">");
                            out.print("&lt;!-");
                            int prevChar = c;
                            while ((c = in.read()) != -1) {
                                printChar(out, c);
                                if(prevChar == '-' && c == '>') {
                                    break;
                                }
                                prevChar = c;
                            }
                            out.print("</SPAN>");
                        } else {
                            out.print("&lt;!");
                            if (isAlpha(c)) {
                                 buff[currentPos++] = (char) c;
                            } else {
                                printChar(out, c);
                            }
                        }
                    } else {
                        out.print("&lt;");
                        if (isAlpha(c)) {
                             buff[currentPos++] = (char) c;
                        } else {
                             printChar(out, c);
                        }
                    }
                } else {
                    printChar(out, c);
                }
            }
        }
        flushPrinter(out);
        writeEpilog(out);
        in.close();
    }
    //</Protectd_Methods>


    //<Public_Methods>
    /**
    Copy private attributes of this SgmlURL to another file.
    @param pFile SgmlURL to which the attributes is to be copied.
    */
    public void copyAttributesTo(SgmlURL pURL) {
        pURL.showLineNumber(bShowLineNumber);
        pURL.lineNumberColor(_colorLineNumber);
        pURL.backgroundColor(_colorBackground);
        pURL.textColor(_colorText);
        pURL.descriptionColor(_colorDescription);
        pURL.stringColor(_colorString);
        pURL.entityColor(_colorEntity);
        pURL.commentColor(_colorComment);
        pURL.keywordColor(_colorKeyword);
        pURL.userKeywordColor(_colorUserKeyword);
        pURL.boldString(_boldString);
        pURL.boldEntity(_boldEntity);
        pURL.boldComment(_boldComment);
        pURL.boldKeyword(_boldKeyword);
        pURL.boldUserKeyword(_boldUserKeyword);
        pURL.descriptionPhrase(_descriptionPhrase);
    }
    /**
    Copy private attributes from another SgmlURL.
    @param pURL SgmlURL from which the attributes is to be copied.
    */
    public void copyAttributesFrom(SgmlURL pURL) {
        bShowLineNumber = pURL.showLineNumber();
        _colorLineNumber = pURL.lineNumberColor();
        _colorBackground = pURL.backgroundColor();
        _colorText = pURL.textColor();
        _colorDescription = pURL.descriptionColor();
        _colorString = pURL.stringColor();
        _colorEntity = pURL.entityColor();
        _colorComment = pURL.commentColor();
        _colorKeyword = pURL.keywordColor();
        _colorUserKeyword = pURL.userKeywordColor();
        _boldString = pURL.boldString();
        _boldEntity = pURL.boldEntity();
        _boldComment = pURL.boldComment();
        _boldKeyword = pURL.boldKeyword();
        _boldUserKeyword = pURL.boldUserKeyword();
        _descriptionPhrase = pURL.descriptionPhrase();
    }

    /**
    Toggle line number mode.
    @param bShow If true, show line numbers.
    */
    public void showLineNumber(boolean bShow) {
        bShowLineNumber = bShow;
    }
    /**
    Get the current line number mode.
    */
    public boolean showLineNumber() {
        return bShowLineNumber;
    }

    /**
    Set background color to _color.
    @param _color New background color.
    Use the color name or 6 hexadecimal numbers (e.g. #00ff00 indicates green).
    */
    public void backgroundColor(String _color) {
        _colorBackground = _color;
    }
    /**
    Get the current background color.
    @return Background color.
    */
    public String backgroundColor() {
        return _colorBackground;
    }
    /**
    Set text color to _color.
    @param _color New text color.
    Use the color name or 6 hexadecimal numbers (e.g. #00ff00 indicates green).
    */
    public void textColor(String _color) {
        _colorText = _color;
    }
    /**
    Get the current text color.
    @return Text color.
    */
    public String textColor() {
        return _colorText;
    }
    /**
    Set description phrase color to _color.
    @param _color New description phrase color.
    Use the color name or 6 hexadecimal numbers (e.g. #00ff00 indicates green).
    */
    public void descriptionColor(String _color) {
        _colorDescription = _color;
    }
    /**
    Get the current description phrase color.
    @return Description phrase color.
    */
    public String descriptionColor() {
        return _colorDescription;
    }
    /**
    Set line number color to _color.
    @param _color New line number color.
    Use the color name or 6 hexadecimal numbers (e.g. #00ff00 indicates green).
    */
    public void lineNumberColor(String _color) {
        _colorLineNumber = _color;
    }
    /**
    Get the current line number color.
    @return Line number color.
    */
    public String lineNumberColor() {
        return _colorLineNumber;
    }
    /**
    Set string color to _color.
    @param _color New string color.
    Use the color name or 6 hexadecimal numbers (e.g. #00ff00 indicates green).
    */
    public void stringColor(String _color) {
        _colorString = _color;
    }
    /**
    Get the current string color.
    @return String color.
    */
    public String stringColor() {
        return _colorString;
    }
    /**
    Set entity color to _color.
    Currently, same color is used for both
    parameter entities and character entities.
    @param _color New entity color.
    Use the color name or 6 hexadecimal numbers (e.g. #00ff00 indicates green).
    */
    public void entityColor(String _color) {
        _colorEntity = _color;
    }
    /**
    Get the current entity color.
    Currently, same color is used for both
    parameter entities and character entities.
    @return Entity color.
    */
    public String entityColor() {
        return _colorEntity;
    }
    /**
    Set comment color to _color.
    @param _color New comment color.
    Use the color name or 6 hexadecimal numbers (e.g. #00ff00 indicates green).
    */
    public void commentColor(String _color) {
        _colorComment = _color;
    }
    /**
    Get the current comment color.
    @return Comment color
    */
    public String commentColor() {
        return _colorComment;
    }
    /**
    Set keyword color to _color.
    @param _color New keyword color.
    Use the color name or 6 hexadecimal numbers (e.g. #00ff00 indicates green).
    */
    public void keywordColor(String _color) {
        _colorKeyword = _color;
    }
    /**
    Get the current keyword color.
    @return Keyword color
    */
    public String keywordColor() {
        return _colorKeyword;
    }
    /**
    Set user-defined keyword color to _color.
    @param _color New user-defined keyword color.
    Use the color name or 6 hexadecimal numbers (e.g. #00ff00 indicates green).
    */
    public void userKeywordColor(String _color) {
        _colorUserKeyword = _color;
    }
    /**
    Get the current user-defined keyword color.
    @return User-defined keyword color
    */
    public String userKeywordColor() {
        return _colorUserKeyword;
    }

    /**
    Add a new keyword to the set of user-defined keywords.
    Note that user-defined keywords take precedence over sgml keywords.
    @param keyword New keyword to be added to the set.
    */
    public void addUserKeywords(String keyword) {
        UserKeywords.add(keyword);
    }
    /**
    Add a new keyword to the set of user-defined keywords.
    Note that user-defined keywords take precedence over sgml keywords.
    @param keywords New keyword array to be added to the set.
    */
    public void addUserKeywords(String[] keywords) {
        for(int i=0;i<keywords.length;i++) {
            UserKeywords.add(keywords[i]);
        }
    }
    /**
    Clear the set of user-defined keywords.
    Note that user-defined keywords take precedence over sgml keywords.
    */
    public void clearUserKeywordSet() {
        UserKeywords.clear();
    }
    /**
    Get the set of user-defined keywords.
    Note that user-defined keywords take precedence over sgml keywords.
    */
    public String[] userKeywordSet() {
        return (String[]) UserKeywords.toArray();
    }
    /**
    Set the set of user-defined keywords.
    Note that user-defined keywords take precedence over sgml keywords.
    WARNING: It automatically removes all existing keywords.
    If you'd like to keep the old keywords, use addUserKeywords(String[]).
    */
    public void userKeywordSet(String[] keywords) {
        UserKeywords.clear();
        for(int i=0;i<keywords.length;i++) {
            UserKeywords.add(keywords[i]);
        }
    }

    /**
    Toggle string font weight (bold vs plain).
    @param _bold If true, set strings to bold.
    */
    public void boldString(boolean _bold) {
        _boldString = _bold;
    }
    /**
    Get the current font weight of strings.
    @return String font weight.
    */
    public boolean boldString() {
        return _boldString;
    }
    /**
    Toggle entity font weight (bold vs plain).
    @param _bold If true, set entities to bold.
    */
    public void boldEntity(boolean _bold) {
        _boldEntity = _bold;
    }
    /**
    Get the current font weight of entities.
    @return Entity font weight.
    */
    public boolean boldEntity() {
        return _boldEntity;
    }
    /**
    Toggle comment font weight (bold vs plain).
    @param _bold If true, set comments to bold.
    */
    public void boldComment(boolean _bold) {
        _boldComment = _bold;
    }
    /**
    Get the current font weight of comments.
    @return Comment font weight.
    */
    public boolean boldComment() {
        return _boldComment;
    }
    /**
    Toggle keyword font weight (bold vs plain).
    @param _bold If true, set keywords to bold.
    */
    public void boldKeyword(boolean _bold) {
        _boldKeyword = _bold;
    }
    /**
    Get the current font weight of the keywords.
    @return Keyword font weight.
    */
    public boolean boldKeyword() {
        return _boldKeyword;
    }
    /**
    Toggle user-defined keyword font weight (bold vs plain).
    @param _bold If true, set user-defined keywords to bold.
    */
    public void boldUserKeyword(boolean _bold) {
        _boldUserKeyword = _bold;
    }
    /**
    Get the current font weight of the user-defined keywords.
    @return User-defined keyword font weight.
    */
    public boolean boldUserKeyword() {
        return _boldUserKeyword;
    }

    /**
    Get the current description phrase.
    @return Description phrase.
    */
    public String descriptionPhrase() {
        return _descriptionPhrase;
    }
    /**
    Set the description phrase.
    @param newDescription New description phrase.
    */
    public void descriptionPhrase(String newDescription) {
        _descriptionPhrase = newDescription;
    }

    /**
    Returns the string representation of fileURL.
    */
    public String toString() {
        //return fileURL.toExternalForm();
        return fileURL.getFile();
    }

    /**
    Htmlize the file specified by the URL and write its html-ized file to the standard output
    This URL should be a SgmlURL.
    */
    public void htmlize() {
        // write to the standard output
        try {
            PrintWriter out =
                new PrintWriter(
                    new BufferedWriter(
                        new OutputStreamWriter(System.out)));
            writeHtml(out);
            out.close();
        } catch(IOException exc) {
            exc.printStackTrace();
        }
    }

    /**
    Write the file specified by the URL to the PrintWriter as Html-ized format
    This URL should be a SgmlURL.
    @param out PrintWriter to which the output is to be written.
    */
    public void htmlize(PrintWriter out) {
        try {
            writeHtml(out);
            out.close();
        } catch(IOException exc) {
            exc.printStackTrace();
        }
    }

    /**
    Htmlize the file specified by the URL and write its html-ized output to the destination.
    This URL should be a SgmlURL, and the destination should be an html file.
    WARNING: If the same name as the destination file exists, it OVERWRITES!
    @param destPath Destination file.
    */
    public void htmlize(String destPath) {
        if(destPath == null) {
            // ???
        }
        try {
            PrintWriter out =
                new PrintWriter(
                    new BufferedWriter(
                        new FileWriter(destPath)));
            writeHtml(out);
            out.close();
        } catch(IOException exc) {
            exc.printStackTrace();
        }
    }
    //</Public_Methods>


    //<Main_Method>
    /**
    This function is provided for testing purposes.
    Please use or modify SgmlURL2Html class.
    Main method takes the name of the URL to be converted,
    and it writes its HTML-ized content to the standard output.
    No error checking is done.
    */
    public static void main(String[] args) {
        SgmlURL pURL;
        try {
            pURL = new SgmlURL(args[0]);
            pURL.htmlize();  // write it to the standard output
        } catch(MalformedURLException exc) {
            exc.printStackTrace();
        }
    }
    //</Main_Method>
}

