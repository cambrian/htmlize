//////////////////////////////////////////////////////////////////////////
// The contents of this file are subject to the Mozilla Public License
// Version 1.0 (the "License"); you may not use this file except in
// compliance with the License. You may obtain a copy of the License at
// http://www.mozilla.org/MPL/
//
// Software distributed under the License is distributed on an "AS IS"
// basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
// License for the specific language governing rights and limitations under
// the License.
//
// The Original Code is Html-ization Utility Classes in Java.
//
// The Initial Developer of the Original Code is Hyoungsoo Yoon.
// Portions created by Hyoungsoo Yoon are
// Copyright (C) 1999 Hyoungsoo Yoon.  All Rights Reserved.
//
// Contributor(s):
//
//////////////////////////////////////////////////////////////////////////


/*
  Html-ization Utility Classes

  @copyright  Hyoungsoo Yoon
  @date  Sep 6th, 1999
*/
package com.bluecraft.htmlize;

import java.io.*;


/**
  SgmlFilenameFilter implements FilenameFilter.
  Accepted extensions: html, htm, shtml, shtm, jhtml, jhtm, sgml, sgm,
                       asp, cfm, htx, php, jsp, wml, xml.

  @author  Hyoungsoo Yoon
  @version  1.1
*/
public class SgmlFilenameFilter
    extends AbstractFilenameFilter
{
    public SgmlFilenameFilter() {
        super();
    }

    public SgmlFilenameFilter(String[] names) {
        super(names);
    }

    // Accept all "SGML-like" and "XML-like" files
    public boolean accept(File dir, String name) {
        if(!super.accept(dir, name)) {
            return false;
        }

        File file = new File(dir,name);
        if(file.isDirectory()) {
            return true;
        } else if(file.isFile() &&
            (name.endsWith(".html") || name.endsWith(".htm")
             || name.endsWith(".shtml") || name.endsWith(".shtm")
             || name.endsWith(".jhtml") || name.endsWith(".jhtm")
             || name.endsWith(".sgml") || name.endsWith(".sgm")
             || name.endsWith(".asp")
             || name.endsWith(".cfm")
             || name.endsWith(".htx")
             || name.endsWith(".php")
             || name.endsWith(".jsp")
             || name.endsWith(".wml")
             || name.endsWith(".xml")
        )) {
            return true;
        } else {
            return false;
        }
    }

}


