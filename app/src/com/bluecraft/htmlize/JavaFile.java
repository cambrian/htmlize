//////////////////////////////////////////////////////////////////////////
// The contents of this file are subject to the Mozilla Public License
// Version 1.0 (the "License"); you may not use this file except in
// compliance with the License. You may obtain a copy of the License at
// http://www.mozilla.org/MPL/
//
// Software distributed under the License is distributed on an "AS IS"
// basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
// License for the specific language governing rights and limitations under
// the License.
//
// The Original Code is Html-ization Utility Classes in Java.
//
// The Initial Developer of the Original Code is Hyoungsoo Yoon.
// Portions created by Hyoungsoo Yoon are
// Copyright (C) 1999 Hyoungsoo Yoon.  All Rights Reserved.
//
// Contributor(s):
//
//////////////////////////////////////////////////////////////////////////


/*
  Html-ization Utility Classes

  @copyright  Hyoungsoo Yoon
  @date  Sep 6th, 1999
*/
package com.bluecraft.htmlize;

import java.io.*;
import java.util.*;


/**
  JavaFile extends ProgramFile which implements Htmlizable.
  This class generates HTML markuped output of its file content
  through htmlize(...) functions.
  It converts all program files in the source path
  (recursively, if a directory is given) into HTML files.

  @author  Hyoungsoo Yoon
  @version  1.1
*/
public class JavaFile extends ProgramFile {

    //<Static_Fields>
    private static String strKeywords =
        "abstract boolean break byte byvalue case cast " +
        "catch char class const continue default do double " +
        "else extends false final finally float for future " +
        "generic goto if implements import inner instanceof " +
        "int interface long native new null operator outer " +
        "package private protected public rest return short " +
        "static super switch synchronized this throw throws " +
        "transient true try var void volatile while ";
    //</Static_Fields>


    //<Private_Fields>
    //</Private_Fields>


    //<Protectd_Fields>
    //</Protectd_Fields>


    //<Public_Fields>
    //</Public_Fields>


    //<Constructors>
    /**
    Construct JavaFile
    @param parent Pathname in which the file is created.
    @param name Name of the file to be created.
    */
    public JavaFile(String parent, String name) {
        super(parent, name);
        initialize();
    }
    /**
     * @param parent Pathname in which the file is created.
     * @param name Name of the file to be created.
     * @param blackList Names of files/dirs to be pre-filtered out.
     */
    public JavaFile(String parent, String name, String[] blackList) {
        super(parent, name, blackList);
        initialize();
    }
    /**
    Construct JavaFile
    @param parent File object in which the file is created.
    @param name Name of the file to be created.
    */
    public JavaFile(File parent, String name) {
        super(parent, name);
        initialize();
    }
    /**
     * @param parent File object in which the file is created.
     * @param name Name of the file to be created.
     * @param blackList Names of files/dirs to be pre-filtered out.
     */
    public JavaFile(File parent, String name, String[] blackList) {
        super(parent, name, blackList);
        initialize();
    }
    /**
    Construct JavaFile
    @param pathname Full pathname of the file to be created.
    */
    public JavaFile(String pathname) {
        super(pathname);
        initialize();
    }
    /**
     * @param pathname Full pathname of the file to be created.
     * @param blackList Names of files/dirs to be pre-filtered out.
     */
    public JavaFile(String pathname, String[] blackList) {
        super(pathname, blackList);
        initialize();
    }
    //</Constructors>


    //<Private_Methods>
    private void initialize() {
        StringTokenizer st = new StringTokenizer(strKeywords);
        while (st.hasMoreTokens()) {
            KEYWORDS.add(st.nextToken());
        }
    }
    //</Private_Methods>


    //<Protectd_Methods>
    //</Protectd_Methods>


    //<Public_Methods>
    /**
    Htmlize the file and write its html file to the standard output
    This file should be a java file.
    */
    public void htmlize() {
        FileFilter javaFilter = new JavaFileFilter(BlackList);
        if(!isFile() || !javaFilter.accept(this)) {
            System.err.println("Source path name not valid");
            return;
        }

        // write to the standard output
        try {
            PrintWriter out =
                new PrintWriter(
                    new BufferedWriter(
                        new OutputStreamWriter(System.out)));
            writeHtml(out);
            out.close();
        } catch(FileNotFoundException exc) {
            System.out.println("File Not Found: " + getName());
        } catch(IOException exc) {
            exc.printStackTrace();
        }
    }

    /**
    Write this file to the PrintWriter as Html file
    This file should be a java file.
    @param out PrintWriter to which the output is to be written.
    */
    public void htmlize(PrintWriter out) {
        FileFilter javaFilter = new JavaFileFilter(BlackList);
        if(!isFile() || !javaFilter.accept(this)) {
            System.err.println("Source path name not valid");
            return;
        }

        try {
            writeHtml(out);
            out.close();
        } catch(FileNotFoundException exc) {
            System.out.println("File Not Found: " + getName());
        } catch(IOException exc) {
            exc.printStackTrace();
        }
    }

    /**
    Htmlize the file and write its html file to the destination.
    If this file is a directory, the destination should also be a directory.
    If this file is a java file, the destination should be an html file,
    or a directory to which the converted file is to be written.
    WARNING: If the same name as the destination file exists, it OVERWRITES!
    @param destPath Destination file or directory
    */
    public void htmlize(String destPath) {
        if(destPath == null) {
            // set destPath to the current directory if "this" is dir
            // or set it to the current directory + name - ".java" + ".html"
        }

        FileFilter javaFilter = new JavaFileFilter(BlackList);
        if(isDirectory()) {
            String[] childList = list(new JavaFilenameFilter(BlackList));
            File dFile = new File(destPath); // should be directory!!!!
            if(!dFile.exists()) {
                dFile.mkdir();
            }
            if(!dFile.isDirectory()) {
                System.err.println("Destination path name not valid: " +
                    dFile.getPath() + " should be a directory.");
                return;
            }

            generateIndexFile(dFile,childList);

            for(int i=0;i<childList.length;i++) {
                JavaFile jFile = new JavaFile(getPath(),childList[i],BlackList);
                copyAttributesTo(jFile);
                if(jFile.isDirectory()) {
                    jFile.htmlize(dFile.getPath() + separator + childList[i]);
                } else {
                    int dot = childList[i].lastIndexOf('.');
                    String htmlName = childList[i].substring(0,dot) + "_java.html";
                    jFile.htmlize(dFile.getPath() + separator + htmlName);
                }
            }
            return;
        } else if(isFile() && javaFilter.accept(this)) {
            JavaFile dstFile = new JavaFile(destPath,BlackList);
        if(dstFile.isDirectory()) {
                int dot = getName().lastIndexOf('.');
                String htmlName = getName().substring(0,dot) + "_java.html";
                destPath = dstFile.getPath() + separator + htmlName;
            } else if(!destPath.endsWith(".htm")
                && !destPath.endsWith(".html")) {
                System.err.println("Destination file name not valid");
                return;
            }
        } else {
            System.err.println("Source path name not valid");
            return;
        }

        try {
            PrintWriter out =
                new PrintWriter(
                    new BufferedWriter(
                        new FileWriter(destPath)));
            writeHtml(out);
            out.close();
        } catch(FileNotFoundException exc) {
            System.out.println("File Not Found: " + getPath());
        } catch(IOException exc) {
            exc.printStackTrace();
        }
    }
    //</Public_Methods>


    //<Main_Method>
    /**
    This function is provided for testing purposes.
    Please use or modify JavaToHtml class.
    Main method takes the name of a Java file or directory to be converted,
    it writes its HTML-ized content to the standard output.
    No error checking is done.
    */
    public static void main(String[] args) {
        JavaFile jFile = new JavaFile(args[0]);
        jFile.htmlize();  // write it to the standard output
    }
    //</Main_Method>
}

