//////////////////////////////////////////////////////////////////////////
// The contents of this file are subject to the Mozilla Public License
// Version 1.0 (the "License"); you may not use this file except in
// compliance with the License. You may obtain a copy of the License at
// http://www.mozilla.org/MPL/
//
// Software distributed under the License is distributed on an "AS IS"
// basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
// License for the specific language governing rights and limitations under
// the License.
//
// The Original Code is Html-ization Utility Classes in Java.
//
// The Initial Developer of the Original Code is Hyoungsoo Yoon.
// Portions created by Hyoungsoo Yoon are
// Copyright (C) 1999 Hyoungsoo Yoon.  All Rights Reserved.
//
// Contributor(s):
//
//////////////////////////////////////////////////////////////////////////


/*
  Html-ization Utility Classes

  @copyright  Hyoungsoo Yoon
  @date  Sep 6th, 1999
*/
package com.bluecraft.htmlize;

import java.io.*;
import java.util.*;
import java.net.*;


/**
  CppURL extends ProgramURL which implements Htmlizable.
  CppURL is a URL to a Cpp program file.
  This class generates HTML markup-ed output of its file content
  through htmlize(...) functions.

  @author  Hyoungsoo Yoon
  @version  1.1
*/
public class CppURL extends ProgramURL {

    //<Static_Fields>
    private static String strKeywords =
        "and and_eq asm auto bitand bitor " +
        "bool break case catch char class " +
        "compl const const_cast continue defulat delete " +
        "do double dynamic_cast else enum explicit " +
        "export extern false float for friend " +
        "goto if inline int long mutable " +
        "namespace new not not_eq operator or " +
        "or_eq private protected public register reinterpret_cast " +
        "return short signed sizeof static static_cast " +
        "struct switch template this throw true " +
        "try typedef typeid typename union unsigned " +
        "using virtual void volatile wchar_t while " +
        "xor xor_eq " +
        // "main " +
        "#include #define #ifdef #undef #endif #line #error #pragma ";
    //</Static_Fields>


    //<Private_Fields>
    //</Private_Fields>


    //<Protectd_Fields>
    //</Protectd_Fields>


    //<Public_Fields>
    //</Public_Fields>


    //<Constructors>
    /**
    Construct CppURL from the string representation of URL.
    @param spec - the String to parse as a URL.
    @throws MalformedURLException - If the string specifies an unknown protocol.
    */
    public CppURL(String spec) throws MalformedURLException {
        this(new URL(spec));
    }
    /**
    Construct CppURL from the URL. It uses the predefined keyword set.
    @param url - the URL specifying the cpp program file.
    */
    public CppURL(URL url) {
        this(url, strKeywords);
    }
    /**
    Construct CppURL from the URL and the keyword list.
    Note that this constructor is protected.
    @param url - the URL specifying the cpp program file.
    @param keywordsList - Keyword list as a concatenated string.
    */
    protected CppURL(URL url, String keywordsList) {
        super(url, keywordsList);
    }
    //</Constructors>


    //<Private_Methods>
    //</Private_Methods>


    //<Protectd_Methods>
    protected boolean isAlpha(int c) {
        return ((c >= 'a' && c <= 'z') ||
            (c >= 'A' && c <= 'Z') || (c == '_') || (c == '#'));
    }
    //</Protectd_Methods>


    //<Public_Methods>
    //</Public_Methods>


    //<Main_Method>
    /**
    This function is provided for testing purposes.
    Please use or modify CppURL2Html class.
    Main method takes the name of a URL to be converted,
    and it writes its HTML-ized content to the standard output.
    No error checking is done.
    */
    public static void main(String[] args) {
        CppURL pURL;
        try {
            pURL = new CppURL(args[0]);
            pURL.htmlize();  // write it to the standard output
        } catch(MalformedURLException exc) {
            exc.printStackTrace();
        }
    }
    //</Main_Method>
}

