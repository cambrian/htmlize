//////////////////////////////////////////////////////////////////////////
// The contents of this file are subject to the Mozilla Public License
// Version 1.0 (the "License"); you may not use this file except in
// compliance with the License. You may obtain a copy of the License at
// http://www.mozilla.org/MPL/
//
// Software distributed under the License is distributed on an "AS IS"
// basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
// License for the specific language governing rights and limitations under
// the License.
//
// The Original Code is Html-ization Utility Classes in Java.
//
// The Initial Developer of the Original Code is Hyoungsoo Yoon.
// Portions created by Hyoungsoo Yoon are
// Copyright (C) 2002 Hyoungsoo Yoon.  All Rights Reserved.
//
// Contributor(s):
//
//////////////////////////////////////////////////////////////////////////


/*
  Html-ization Utility Classes

  @copyright  Hyoungsoo Yoon
  @date  Jul 26th, 2002
 */
package com.bluecraft.htmlize;

import java.io.*;
import java.util.*;


/**
  CsharpFile extends ProgramFile which implements Htmlizable.
  This class generates HTML markuped output of its file content
  through htmlize(...) functions.
  It converts all program files in the source path
  (recursively, if a directory is given) into HTML files.

  @author  Hyoungsoo Yoon
  @version  1.1
*/
public class CsharpFile
    extends ProgramFile
{

    //<Static_Fields>
    private static String strKeywords =
        "abstract as base bool break byte case catch char " +
        "checked class const continue decimal default delegate do double else enum event explicit " +
        "extern false finally fixed float for foreach get goto if implicit in int interface " +
        "internal is lock long namespace new null object operator out override params private " +
        "protected public readonly ref return sbyte sealed set short sizeof stackalloc static string struct " +
        "switch this throw true try typeof uint ulong unchecked unsafe ushort using value virtual " +
        "void while " +
        // "main " +
        "#define #if #ifdef #undef #else #elif #endif #line #warning #error #region #endregion ";
    //</Static_Fields>


    //<Private_Fields>
    //</Private_Fields>


    //<Protectd_Fields>
    //</Protectd_Fields>


    //<Public_Fields>
    //</Public_Fields>


    //<Constructors>
    /**
    Construct CsharpFile
    @param parent Pathname in which the file is created.
    @param name Name of the file to be created.
    */
    public CsharpFile(String parent, String name) {
        super(parent, name);
        initialize();
    }
    /**
     * @param parent Pathname in which the file is created.
     * @param name Name of the file to be created.
     * @param blackList Names of files/dirs to be pre-filtered out.
     */
    public CsharpFile(String parent, String name, String[] blackList) {
        super(parent, name, blackList);
        initialize();
    }
    /**
    Construct CsharpFile
    @param parent File object in which the file is created.
    @param name Name of the file to be created.
    */
    public CsharpFile(File parent, String name) {
        super(parent, name);
        initialize();
    }
    /**
     * @param parent File object in which the file is created.
     * @param name Name of the file to be created.
     * @param blackList Names of files/dirs to be pre-filtered out.
     */
    public CsharpFile(File parent, String name, String[] blackList) {
        super(parent, name, blackList);
        initialize();
    }
    /**
    Construct CsharpFile
    @param pathname Full pathname of the file to be created.
    */
    public CsharpFile(String pathname) {
        super(pathname);
        initialize();
    }
    /**
     * @param pathname Full pathname of the file to be created.
     * @param blackList Names of files/dirs to be pre-filtered out.
     */
    public CsharpFile(String pathname, String[] blackList) {
        super(pathname, blackList);
        initialize();
    }
    //</Constructors>


    //<Private_Methods>
    private void initialize() {
        StringTokenizer st = new StringTokenizer(strKeywords);
        while (st.hasMoreTokens()) {
            KEYWORDS.add(st.nextToken());
        }
    }
    //</Private_Methods>


    //<Protectd_Methods>
    protected boolean isAlpha(int c) {
        return ((c >= 'a' && c <= 'z') ||
            (c >= 'A' && c <= 'Z') || (c == '_') || (c == '#'));
    }
    //</Protectd_Methods>


    //<Public_Methods>
    /**
    Htmlize the file and write its html file to the standard output
    This file should be a Csharp file.
    */
    public void htmlize() {
        FileFilter cppFilter = new CsharpFileFilter(BlackList);
        if(!isFile() || !cppFilter.accept(this)) {
            System.err.println("Source path name not valid");
            return;
        }

        // write to the standard output
        try {
            PrintWriter out =
                new PrintWriter(
                    new BufferedWriter(
                        new OutputStreamWriter(System.out)));
            writeHtml(out);
            out.close();
        } catch(FileNotFoundException exc) {
            System.out.println("File Not Found: " + getName());
        } catch(IOException exc) {
            exc.printStackTrace();
        }
    }

    /**
    Write this file to the PrintWriter as Html file
    This file should be a Csharp file.
    @param out PrintWriter to which the output is to be written.
    */
    public void htmlize(PrintWriter out) {
        FileFilter cppFilter = new CsharpFileFilter(BlackList);
        if(!isFile() || !cppFilter.accept(this)) {
            System.err.println("Source path name not valid");
            return;
        }

        try {
            writeHtml(out);
            out.close();
        } catch(FileNotFoundException exc) {
            System.out.println("File Not Found: " + getName());
        } catch(IOException exc) {
            exc.printStackTrace();
        }
    }

    /**
    Htmlize the file and write its html file to the destination.
    If this file is a directory, the destination should also be a directory.
    If this file is a Csharp file, the destination should be an html file,
    or a directory to which the converted file is to be written.
    WARNING: If the same name as the destination file exists, it OVERWRITES!
    @param destPath Destination file or directory
    */
    public void htmlize(String destPath) {
        if(destPath == null) {
            // set destPath to the current directory if "this" is dir
            // or set it to the current directory + name - ".cpp" + ".html"
        }

        FileFilter cppFilter = new CsharpFileFilter(BlackList);
        if(isDirectory()) {
            String[] childList = list(new CsharpFilenameFilter(BlackList));
            File dFile = new File(destPath); // should be directory!!!!
            if(!dFile.exists()) {
                dFile.mkdir();
            }
            if(!dFile.isDirectory()) {
                System.err.println("Destination path name not valid: " +
                    dFile.getPath() + " should be a directory.");
                return;
            }

            generateIndexFile(dFile,childList);

            for(int i=0;i<childList.length;i++) {
                CsharpFile cFile = new CsharpFile(getPath(),childList[i], BlackList);
                copyAttributesTo(cFile);
                if(cFile.isDirectory()) {
                    cFile.htmlize(dFile.getPath() + separator + childList[i]);
                } else {
                    int dot = childList[i].lastIndexOf('.');
                    String htmlName = childList[i].substring(0,dot) +
                        "_" + childList[i].substring(dot+1) + ".html";
                    cFile.htmlize(dFile.getPath() + separator + htmlName);
                }
            }
            return;
        } else if(isFile() && cppFilter.accept(this)) {
            CsharpFile dstFile = new CsharpFile(destPath, BlackList);
            if(dstFile.isDirectory()) {
                int dot = getName().lastIndexOf('.');
                String htmlName = getName().substring(0,dot) +
                    "_" + getName().substring(dot+1) + ".html";
                destPath = dstFile.getPath() + separator + htmlName;
            } else if(!destPath.endsWith(".htm")
                && !destPath.endsWith(".html")) {
                System.err.println("Destination file name not valid");
                return;
            }
        } else {
            System.err.println("Source path name not valid");
            return;
        }

        try {
            PrintWriter out =
                new PrintWriter(
                    new BufferedWriter(
                        new FileWriter(destPath)));
            writeHtml(out);
            out.close();
        } catch(FileNotFoundException exc) {
            System.out.println("File Not Found: " + getPath());
        } catch(IOException exc) {
            exc.printStackTrace();
        }
    }
    //</Public_Methods>


    //<Main_Method>
    /**
    This function is provided for testing purposes.
    Please use or modify CsharpToHtml class.
    Main method takes the name of a cpp file or directory to be converted,
    it writes its HTML-ized content to the standard output.
    No error checking is done.
    */
    public static void main(String[] args) {
        CsharpFile cFile = new CsharpFile(args[0]);
        cFile.htmlize();  // write it to the standard output
    }
    //</Main_Method>
}


