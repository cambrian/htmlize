//////////////////////////////////////////////////////////////////////////
// The contents of this file are subject to the Mozilla Public License
// Version 1.0 (the "License"); you may not use this file except in
// compliance with the License. You may obtain a copy of the License at
// http://www.mozilla.org/MPL/
//
// Software distributed under the License is distributed on an "AS IS"
// basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
// License for the specific language governing rights and limitations under
// the License.
//
// The Original Code is Html-ization Utility Classes in Java.
//
// The Initial Developer of the Original Code is Hyoungsoo Yoon.
// Portions created by Hyoungsoo Yoon are
// Copyright (C) 1999 Hyoungsoo Yoon.  All Rights Reserved.
//
// Contributor(s):
//
//////////////////////////////////////////////////////////////////////////


/*
  Html-ization Utility Classes

  @copyright  Hyoungsoo Yoon
  @date  Sep 6th, 1999
*/
package com.bluecraft.htmlize;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;


/**
  ProgramToHtml Conversion Servlet.
  This is an example program which utilizes ProgramFile class,
  which implements Htmlizable interface.
  ProgramToHtmlServlet can handle both POST and GET requests from clients,
  which are equivalently implemented following simple example programs
  included in JSDK 2.0.

  @author  Hyoungsoo Yoon
  @version  1.1
*/
public class ProgramToHtmlServlet extends HttpServlet
    implements SingleThreadModel {

    String resultsDir;
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    /**
    Attribute name "programfile" is used to retrieve the desired file name.
    */
    public void doPost(HttpServletRequest req, HttpServletResponse res)
    throws ServletException, IOException
    {
        // first, set the "content type" header of the response
        res.setContentType("text/html");

        // Get the response's PrintWriter to return text to the client.
        PrintWriter toClient = res.getWriter();

        // Open a file to be converted to HTML.
        String fileName = req.getParameterValues("programfile")[0];

        // Open the file, set some options, and produce HTML output.
        ProgramFile cFile = new ProgramFile(fileName);
        cFile.showLineNumber(true);
        cFile.commentColor("green");
        cFile.htmlize(toClient);

        // Close the writer; the response is done.
        toClient.close();
    }

    /**
    Attribute name "programfile" is used to retrieve the desired file name.
    */
    public void doGet(HttpServletRequest req, HttpServletResponse res)
          throws ServletException, IOException
    {
        // first, set the "content type" header of the response
        res.setContentType("text/html");

        //Get the response's PrintWriter to return text to the client.
        PrintWriter toClient = res.getWriter();

        // Open a file to be converted to HTML.
        String fileName = req.getParameterValues("programfile")[0];

        // Open the file, set some options, and produce HTML output
        ProgramFile cFile = new ProgramFile(fileName);
        cFile.showLineNumber(false);
        cFile.commentColor("green");
        cFile.htmlize(toClient);

        // Close the writer; the response is done.
        toClient.close();
    }
}


