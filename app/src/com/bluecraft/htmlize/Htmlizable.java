//////////////////////////////////////////////////////////////////////////
// The contents of this file are subject to the Mozilla Public License
// Version 1.0 (the "License"); you may not use this file except in
// compliance with the License. You may obtain a copy of the License at
// http://www.mozilla.org/MPL/
//
// Software distributed under the License is distributed on an "AS IS"
// basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
// License for the specific language governing rights and limitations under
// the License.
//
// The Original Code is Html-ization Utility Classes in Java.
//
// The Initial Developer of the Original Code is Hyoungsoo Yoon.
// Portions created by Hyoungsoo Yoon are
// Copyright (C) 1999 Hyoungsoo Yoon.  All Rights Reserved.
//
// Contributor(s):
//
//////////////////////////////////////////////////////////////////////////


/*
  Html-ization Utility Classes

  @copyright  Hyoungsoo Yoon
  @date  Sep 6th, 1999
*/
package com.bluecraft.htmlize;

import java.io.*;

/**
  Htmlizable Interface.
  Classes which implement this interface output relevant data as HTML file.
  Htmlizable is *the* main core class of the package com.bluecraft.htmlize.

  @author  Hyoungsoo Yoon
  @version  1.1
*/
public interface Htmlizable {
    /**
    Write the content as HTML to the standard output.
    */
    public void htmlize();
    /**
    Write the content as HTML to the given PrintWriter.
    @param out PrintWriter to which the HTML output is to be written.
    */
    public void htmlize(PrintWriter out);
    /**
    Write the content as HTML to the specified file.
    @param destinationPath Name of a file to which the HTML output is to be written.
    */
    public void htmlize(String destinationPath);
}

