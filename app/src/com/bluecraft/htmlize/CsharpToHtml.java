//////////////////////////////////////////////////////////////////////////
// The contents of this file are subject to the Mozilla Public License
// Version 1.0 (the "License"); you may not use this file except in
// compliance with the License. You may obtain a copy of the License at
// http://www.mozilla.org/MPL/
//
// Software distributed under the License is distributed on an "AS IS"
// basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
// License for the specific language governing rights and limitations under
// the License.
//
// The Original Code is Html-ization Utility Classes in Java.
//
// The Initial Developer of the Original Code is Hyoungsoo Yoon.
// Portions created by Hyoungsoo Yoon are
// Copyright (C) 2002 Hyoungsoo Yoon.  All Rights Reserved.
//
// Contributor(s):
//
//////////////////////////////////////////////////////////////////////////


/*
  Html-ization Utility Classes

  @copyright  Hyoungsoo Yoon
  @date  Jul 26th, 2002
*/
package com.bluecraft.htmlize;

import java.io.*;
import java.util.*;


/**
  CsharpToHtml Converter.
  This is an example program which utilizes CsharpFile class,
  which implements Htmlizable interface.

  @author  Hyoungsoo Yoon
  @version  1.1
*/
public class CsharpToHtml
{

    /**
    CsharpToHtml takes two arguments.
    First argument: Name of a cpp file or directory to be converted.
    Second argument: Destination file or directory.
    */
    public static void main(String[] args) {
        CsharpFile srcFile;
        String destName;
        if(args.length == 0) {
             System.err.println("Usage: Java com.bluecraft.htmlize.CsharpToHtml CsharpFile HtmlFile/Directory, or");
             System.err.println("       Java com.bluecraft.htmlize.CsharpToHtml Directory Directory");
             return;
        } else {
            // TODO: black list is temporarily hard-coded here.....
            String[] blackList = new String[] {"CVS","bin","obj","doc","Debug","Release","AssemblyInfo.cs","images","alphabets","cards","licenses","licenses_ansi","licenses_unicode","lightBulbs","bjgamesetup"};

             srcFile = new CsharpFile(args[0], blackList);
             if(args.length == 1) {
                 int dot = srcFile.getName().lastIndexOf('.');
                 String htmlName = srcFile.getName().substring(0,dot) +
                     "_" + srcFile.getName().substring(dot+1) + ".html";
                 destName = srcFile.getPath() + File.separator + htmlName;
             } else {
                 destName = args[1];
             }
        }
        // Set some options here.
        srcFile.descriptionPhrase(
        "Generated by <STRONG>com.bluecraft.htmlize</STRONG> HTML-ization Utility Classes.");
        srcFile.showLineNumber(true);  // Show line number....
        // Markup the file and write the output.
        srcFile.htmlize(destName);
    }
}


