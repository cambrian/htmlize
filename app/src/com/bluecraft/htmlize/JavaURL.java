//////////////////////////////////////////////////////////////////////////
// The contents of this file are subject to the Mozilla Public License
// Version 1.0 (the "License"); you may not use this file except in
// compliance with the License. You may obtain a copy of the License at
// http://www.mozilla.org/MPL/
//
// Software distributed under the License is distributed on an "AS IS"
// basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
// License for the specific language governing rights and limitations under
// the License.
//
// The Original Code is Html-ization Utility Classes in Java.
//
// The Initial Developer of the Original Code is Hyoungsoo Yoon.
// Portions created by Hyoungsoo Yoon are
// Copyright (C) 1999 Hyoungsoo Yoon.  All Rights Reserved.
//
// Contributor(s):
//
//////////////////////////////////////////////////////////////////////////


/*
  Html-ization Utility Classes

  @copyright  Hyoungsoo Yoon
  @date  Sep 6th, 1999
*/
package com.bluecraft.htmlize;

import java.io.*;
import java.util.*;
import java.net.*;


/**
  JavaURL extends ProgramURL which implements Htmlizable.
  JavaURL is a URL to a Java program file.
  This class generates HTML markup-ed output of its file content
  through htmlize(...) functions.

  @author  Hyoungsoo Yoon
  @version  1.1
*/
public class JavaURL extends ProgramURL {

    //<Static_Fields>
    private static String strKeywords =
        "abstract boolean break byte byvalue case cast " +
        "catch char class const continue default do double " +
        "else extends false final finally float for future " +
        "generic goto if implements import inner instanceof " +
        "int interface long native new null operator outer " +
        "package private protected public rest return short " +
        "static super switch synchronized this throw throws " +
        "transient true try var void volatile while ";
    //</Static_Fields>


    //<Private_Fields>
    //</Private_Fields>


    //<Protectd_Fields>
    //</Protectd_Fields>


    //<Public_Fields>
    //</Public_Fields>


    //<Constructors>
    /**
    Construct JavaURL from the string representation of URL.
    @param spec - the String to parse as a URL.
    @throws MalformedURLException - If the string specifies an unknown protocol.
    */
    public JavaURL(String spec) throws MalformedURLException {
        this(new URL(spec));
    }
    /**
    Construct JavaURL from the URL. It uses the predefined keyword set.
    @param url - the URL specifying the java program file.
    */
    public JavaURL(URL url) {
        this(url, strKeywords);
    }
    /**
    Construct JavaURL from the URL and the keyword list.
    Note that this constructor is protected.
    @param url - the URL specifying the java program file.
    @param keywordsList - Keyword list as a concatenated string.
    */
    protected JavaURL(URL url, String keywordsList) {
        super(url, keywordsList);
    }
    //</Constructors>


    //<Private_Methods>
    //</Private_Methods>


    //<Protectd_Methods>
    //</Protectd_Methods>


    //<Public_Methods>
    //</Public_Methods>


    //<Main_Method>
    /**
    This function is provided for testing purposes.
    Please use or modify JavaURL2Html class.
    Main method takes the name of a URL to be converted,
    and it writes its HTML-ized content to the standard output.
    No error checking is done.
    */
    public static void main(String[] args) {
        JavaURL pURL;
        try {
            pURL = new JavaURL(args[0]);
            pURL.htmlize();  // write it to the standard output
        } catch(MalformedURLException exc) {
            exc.printStackTrace();
        }
    }
    //</Main_Method>
}

