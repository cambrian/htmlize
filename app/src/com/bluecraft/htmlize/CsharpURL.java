//////////////////////////////////////////////////////////////////////////
// The contents of this file are subject to the Mozilla Public License
// Version 1.0 (the "License"); you may not use this file except in
// compliance with the License. You may obtain a copy of the License at
// http://www.mozilla.org/MPL/
//
// Software distributed under the License is distributed on an "AS IS"
// basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
// License for the specific language governing rights and limitations under
// the License.
//
// The Original Code is Html-ization Utility Classes in Java.
//
// The Initial Developer of the Original Code is Hyoungsoo Yoon.
// Portions created by Hyoungsoo Yoon are
// Copyright (C) 2002 Hyoungsoo Yoon.  All Rights Reserved.
//
// Contributor(s):
//
//////////////////////////////////////////////////////////////////////////


/*
  Html-ization Utility Classes

  @copyright  Hyoungsoo Yoon
  @date  Jul 26th, 2002
*/
package com.bluecraft.htmlize;

import java.io.*;
import java.util.*;
import java.net.*;


/**
  CsharpURL extends ProgramURL which implements Htmlizable.
  CsharpURL is a URL to a Csharp program file.
  This class generates HTML markup-ed output of its file content
  through htmlize(...) functions.

  @author  Hyoungsoo Yoon
  @version  1.1
*/
public class CsharpURL
    extends ProgramURL
{

    //<Static_Fields>
    private static String strKeywords =
        "abstract as base bool break byte case catch char " +
        "checked class const continue decimal default delegate do double else enum event explicit " +
        "extern false finally fixed float for foreach get goto if implicit in int interface " +
        "internal is lock long namespace new null object operator out override params private " +
        "protected public readonly ref return sbyte sealed set short sizeof stackalloc static string struct " +
        "switch this throw true try typeof uint ulong unchecked unsafe ushort using value virtual " +
        "void while " +
        // "main " +
        "#define #if #ifdef #undef #else #elif #endif #line #warning #error #region #endregion ";
    //</Static_Fields>


    //<Private_Fields>
    //</Private_Fields>


    //<Protectd_Fields>
    //</Protectd_Fields>


    //<Public_Fields>
    //</Public_Fields>


    //<Constructors>
    /**
    Construct CsharpURL from the string representation of URL.
    @param spec - the String to parse as a URL.
    @throws MalformedURLException - If the string specifies an unknown protocol.
    */
    public CsharpURL(String spec) throws MalformedURLException {
        this(new URL(spec));
    }
    /**
    Construct CsharpURL from the URL. It uses the predefined keyword set.
    @param url - the URL specifying the cpp program file.
    */
    public CsharpURL(URL url) {
        this(url, strKeywords);
    }
    /**
    Construct CsharpURL from the URL and the keyword list.
    Note that this constructor is protected.
    @param url - the URL specifying the cpp program file.
    @param keywordsList - Keyword list as a concatenated string.
    */
    protected CsharpURL(URL url, String keywordsList) {
        super(url, keywordsList);
    }
    //</Constructors>


    //<Private_Methods>
    //</Private_Methods>


    //<Protectd_Methods>
    protected boolean isAlpha(int c) {
        return ((c >= 'a' && c <= 'z') ||
            (c >= 'A' && c <= 'Z') || (c == '_') || (c == '#'));
    }
    //</Protectd_Methods>


    //<Public_Methods>
    //</Public_Methods>


    //<Main_Method>
    /**
    This function is provided for testing purposes.
    Please use or modify CsharpURL2Html class.
    Main method takes the name of a URL to be converted,
    and it writes its HTML-ized content to the standard output.
    No error checking is done.
    */
    public static void main(String[] args) {
        CsharpURL pURL;
        try {
            pURL = new CsharpURL(args[0]);
            pURL.htmlize();  // write it to the standard output
        } catch(MalformedURLException exc) {
            exc.printStackTrace();
        }
    }
    //</Main_Method>
}


